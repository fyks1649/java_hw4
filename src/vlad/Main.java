package vlad;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Pet pet = new Pet();

        pet.setSpecies("собака");
        pet.setNickname("Бублик");
        pet.setAge(2);
        pet.setTrickLevel(80);
        pet.setHabits(new String[]{"есть", "спать"});

        pet.eat();
        pet.respond();
        pet.foul();
        System.out.println();

        Human human = new Human();

        human.setName("Влад");
        human.setSurname("Летун");
        human.setYear(23);
        human.setIq(80);

        human.greetPet(pet);
        human.describePet(pet);


        Human mother = new Human();

        mother.setName("Елена");
        mother.setSurname("Летун");

        Human father = new Human();

        father.setName("Дмитрий");
        father.setSurname("Летун");

        System.out.println();

        System.out.println(human.toString(mother, father, pet));
        System.out.println(pet.toString());



    }
}

